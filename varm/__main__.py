#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""
It's entry point of program

How it's work:
    1) parse command line arguments
    2) setup logger by this arguments for different modes
    3) setup trash by this arguments
    4) call trash operations
"""


import logging
import varm.trash.trash
import config
import logger
import parser


def setup_loggers(args):
    """
    Setup trash and filesystem logger for silent or regular mode
    :param args: command line arguments. args description in parser module
    """
    logger.setup_logger(
        logging.getLogger('varm.trash'),
        args.logfile,
        args.silent
    )

    logger.setup_logger(
        logging.getLogger('varm.filesystem.operations'),
        args.logfile,
        args.silent
    )


def build_trash_args(args):
    """
    Create dict for trash args for unpacking
    :param args: command line arguments. args description in parser module
    """
    trash_args = {}

    if args.path is not None:
        trash_args['path'] = args.path

    if args.size_limit is not None:
        trash_args['size_limit'] = args.size_limit

    if args.files_limit is not None:
        trash_args['files_limit'] = args.files_limit

    if args.config is not None:
        conf = config.load_config(args.config)
        if conf is not None:
            trash_args = conf

    return trash_args


def run_trash_actions(args):
    """
    Create Trash instance and run trash actions
    :param args: command line arguments. args description in parser module
    """
    custom_trash = varm.trash.trash.Trash(**build_trash_args(args))

    if args.files is not None:
        for filename in args.files:
            custom_trash.insert_by_unix_pattern(filename, args.dry_run)

    if args.recover is not None:
        for filename in args.recover:
            custom_trash.recover_by_unix_pattern(filename, args.dry_run)

    if args.remove is not None:
        for filename in args.remove:
            custom_trash.remove_by_unix_pattern(filename, args.dry_run)

    if args.list:
        for filename in custom_trash.list():
            print filename

    if args.clear:
        custom_trash.clear()


def main():
    """
    Program entry point. It parse command line arguments then setup loggers
    then create trash and call trash actions
    """
    args = parser.get_cli_parser().parse_args()
    setup_loggers(args)
    run_trash_actions(args)


if __name__ == "__main__":
    main()
