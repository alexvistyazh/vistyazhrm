#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import logging
import varm.logger


def test_logger():
    logger = logging.getLogger('varm.trash')
    varm.logger.setup_logger(logger)
    assert len(logger.handlers) == 2
    assert isinstance(logger.handlers[0], logging.FileHandler)
    assert isinstance(logger.handlers[1], logging.StreamHandler)


def test_logger_silent():
    logger = logging.getLogger('varm.trash')
    varm.logger.setup_logger(logger, None, True)
    assert len(logger.handlers) == 1
    assert isinstance(logger.handlers[0], logging.NullHandler)
