#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import varm.parser


def test_parser():
    test_files = '1 2 3'
    raw_args = '{files} --recover {files} --list'.format(files=test_files)
    args = varm.parser.get_cli_parser().parse_args(raw_args.split())
    assert args.files == test_files.split()
    assert args.recover == test_files.split()
    assert args.list


def test_parser_no_remove():
    raw_args = '--list'.split()
    args = varm.parser.get_cli_parser().parse_args(raw_args)
    assert args.list
    assert args.files == []
