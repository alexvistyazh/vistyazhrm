#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


"""
Module Filesystem implement basic OS operations.

Filesystem operations:
    1) iterate by directory
    2) remove directory/file
    3) move directory/file
    4) get directory size
    5) get directory files count

Filesystem observer:
    If you want to handle remove or move operations,
    you should add custom handler on FileObserver class.
    Handler interface:
        1) handle_move(file_path)
        2) handle_remove(old_path, new_path)
"""