#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


"""
FileObserver redirect move and remove operations from filesystem module
to custom handler

Add Handler:
    FileObserver.add_handler(my_handler)

Handler Interface:
    1) handle_remove(file_path)
    2) handle_move(old_path, new_path)
"""


class FileObserver(object):

    """
    Notify about move and remove events filesystem module
    operations set handlers
    """

    handlers = []

    @classmethod
    def add_handler(cls, handler):
        """
        Add handler that was notified about remove
        and move events of filesystem module
        :param handler: handler instance that was notified
        """
        cls.handlers.append(handler)

    @classmethod
    def notify_remove(cls, file_path):
        """
        Notify about remove event filesystem module all handlers
        :param file_path: path of file that was removed
        """
        for handler in cls.handlers:
            if hasattr(handler, 'handle_remove'):
                handler.handle_remove(file_path)

    @classmethod
    def notify_move(cls, old_file_path, new_file_path):
        """
        Notify about move event filesystem module all handlers
        :param old_file_path: old path of files that was moved
        :param new_file_path: new path of file that was moved
        """
        for handler in cls.handlers:
            if hasattr(handler, 'handle_move'):
                handler.handle_move(old_file_path, new_file_path)
