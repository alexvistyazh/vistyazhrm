#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import pytest
import varm.filesystem.fileobserver


@pytest.fixture(scope='function')
def file_observer():

    class ObserverHandler(object):

        def __init__(self):
            self.removed_file_path = None
            self.moved_file_pathes = None

        def handle_remove(self, file_path):
            self.removed_file_path = file_path

        def handle_move(self, old_path, new_path):
            self.moved_file_pathes = (old_path, new_path)

    observer_handler = ObserverHandler()
    varm.filesystem.fileobserver.FileObserver.handlers = [observer_handler]
    return observer_handler


def test_add_handler():
    varm.filesystem.fileobserver.FileObserver.handlers = []
    varm.filesystem.fileobserver.FileObserver.add_handler(object())
    assert len(varm.filesystem.fileobserver.FileObserver.handlers) == 1


def test_notify_move(file_observer):
    moved_files = ('first_moved_file', 'second_moved_file')
    varm.filesystem.fileobserver.FileObserver.notify_move(*moved_files)
    assert file_observer.moved_file_pathes == moved_files


def test_notify_remove(file_observer):
    removed_file = 'removed_file'
    varm.filesystem.fileobserver.FileObserver.notify_remove(removed_file)
    assert file_observer.removed_file_path == removed_file
