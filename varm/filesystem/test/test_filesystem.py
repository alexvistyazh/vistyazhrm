#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import pytest
import varm.filesystem.operations


@pytest.fixture(scope='function')
def dummy_filesystem(tmpdir):

    class DummyFilesystem(object):
        def __init__(self):
            self.move_folder = tmpdir.mkdir('move').join('base')
            self.base_folder = tmpdir.mkdir('base')
            self.base_folder_size = 8192

            self._fill_folder(self.base_folder)

        def _fill_folder(self, folder_path):
            folder_path.join('new_text_file.txt').write('')
            folder_path.mkdir('new_folder')
            folder_path.join('new_folder', 'new_nested_pdf.pdf').write('')

    return DummyFilesystem()


def test_remove(dummy_filesystem):
    remove_path = dummy_filesystem.base_folder
    varm.filesystem.operations.remove_subtree(str(remove_path))
    assert not remove_path.check()


def test_remove_dry_run(dummy_filesystem):
    remove_path = dummy_filesystem.base_folder
    varm.filesystem.operations.remove_subtree(str(remove_path), True)
    assert remove_path.check()


def test_move(dummy_filesystem):
    old_path = dummy_filesystem.base_folder
    new_path = dummy_filesystem.move_folder
    varm.filesystem.operations.move_subtree(str(old_path), str(new_path))
    assert not old_path.check()
    assert new_path.check()


def test_move_dry_run(dummy_filesystem):
    old_path = dummy_filesystem.base_folder
    new_path = dummy_filesystem.move_folder
    varm.filesystem.operations.move_subtree(str(old_path), str(new_path), True)
    assert old_path.check()
    assert not new_path.check()


def test_subtree_files_count(dummy_filesystem):
    test_subtree_path = str(dummy_filesystem.base_folder)
    assert varm.filesystem.operations.count_subtree_files(test_subtree_path) ==\
           len(list(dummy_filesystem.base_folder.visit())) + 1


def test_subtree_files_size(dummy_filesystem):
    test_subtree_path = str(dummy_filesystem.base_folder)
    assert varm.filesystem.operations.count_subtree_size(test_subtree_path) == \
           dummy_filesystem.base_folder_size


def test_is_subdirectory(dummy_filesystem):
    assert varm.filesystem.operations.is_subdirectory(
        str(dummy_filesystem.move_folder),
        str(dummy_filesystem.move_folder.join('nested_file'))
    )


def test_conflict_name_dublicate(dummy_filesystem):
    conflict_file = str(dummy_filesystem.move_folder.join('nested_file'))
    assert varm.filesystem.operations.pick_non_conflict_name(conflict_file) == \
           conflict_file


def test_conflict_name(dummy_filesystem):
    conflict_file = str(dummy_filesystem.base_folder)
    assert varm.filesystem.operations.pick_non_conflict_name(conflict_file) != \
           conflict_file


def test_iterate_subtree(dummy_filesystem):
    iterate_path = dummy_filesystem.base_folder
    iterate_result = list(varm.filesystem.operations.iterate_subtree(
        str(iterate_path))
    )
    assert len(list(dummy_filesystem.base_folder.visit())) + 1 ==\
           len(iterate_result)
