#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


"""
Module Filesystem implement basic OS operations.

Filesystem operations:
    1) iterate by directory
    2) remove directory/file
    3) move directory/file
    4) get directory size
    5) get directory files count
"""


import itertools
import logging
import os
import fileobserver
import collections
import datetime


def iterate_subtree(subtree_path, forward=True):
    """
    Filename generator that iterate on file subtree on filesystem
    :param subtree_path: entry point of iteration
    :param forward: direction of iteration forward or backward
    """
    if os.path.isdir(subtree_path):
        for root, dirnames, filenames in os.walk(subtree_path, forward):
            if forward:
                yield root

            for filename in filenames:
                yield os.path.join(root, filename)

            if not forward:
                yield root
    else:
        yield subtree_path


def remove_subtree(subtree_path, dry_run=False):
    """
    Remove file subtree wholly from filesystem
    :param subtree_path: top of file subtree
    :param dry_run: Mode that ignore remove operations
    :return list of removed files
    """
    removed_files = []
    for filename in iterate_subtree(subtree_path, forward=False):
        if os.path.isdir(filename):
            if remove_directory(filename, dry_run):
                removed_files.append(filename)
        else:
            if remove_file(filename, dry_run):
                removed_files.append(filename)

    return removed_files


def remove_file(file_path, dry_run=False):
    """
    Remove file form filesystem
    :param file_path: path of file that was removed
    :param dry_run: Mode that ignore operations
    :return True if file was removed
    """
    try:
        if not dry_run:
            os.remove(file_path)
            fileobserver.FileObserver.notify_remove(file_path)

        logging.getLogger(__name__).debug(
            'Removed file {0}'.format(file_path)
        )
        return True
    except OSError:
        logging.getLogger(__name__).error(
            'Could not remove file {0}'.format(file_path)
        )
        return False


def remove_directory(directory_path, dry_run=False):
    """
    Remove directory from filesystem
    :param directory_path: path of directory that was removed
    :param dry_run: Mode that ignore operations
    :return True if directory was removed
    """
    try:
        if not dry_run:
            os.rmdir(directory_path)
            fileobserver.FileObserver.notify_remove(directory_path)

        logging.getLogger(__name__).debug(
            'Removed directory {0}'.format(directory_path)
        )

        return True
    except OSError:
        logging.getLogger(__name__).error(
            'Could not remove directory {0}'.format(directory_path)
        )

        return False


def move_subtree(old_subtree_path, new_subtree_path, dry_run=False):
    """
    Move file subtree wholly from filesystem
    :param old_subtree_path: old path of moved file subtree
    :param new_subtree_path: new path of moved file subtree
    :param dry_run: Mode that ignore operations
    :return True if subtree was moved
    """
    old_subtree = list(iterate_subtree(old_subtree_path))
    new_subtree = []

    try:
        if not dry_run:
            os.rename(old_subtree_path, new_subtree_path)
            new_subtree = list(iterate_subtree(new_subtree_path))
            fileobserver.FileObserver.notify_move(
                old_subtree_path,
                new_subtree_path
            )

        logging.getLogger(__name__).debug(
            'Moved file from {0} to {1}'.format(
                old_subtree_path,
                new_subtree_path
            )
        )
    except OSError:
        logging.getLogger(__name__).error(
            'Could not remove from {0} to {1}'.format(
                old_subtree_path,
                new_subtree_path
            )
        )
        return []
    finally:
        return zip(old_subtree, new_subtree)


def count_subtree_size(subtree_path):
    """
    Calculate file subtree size in bytes
    :param subtree_path: path of file subtree
    :return: file subtree size in bytes
    """
    return sum(
        os.path.getsize(filename)
        for filename in iterate_subtree(subtree_path)
    )


def count_subtree_files(subtree_path):
    """
    Calculate file subtree files count
    :param subtree_path: path of file subtree
    :return: number files on subtree
    """
    return sum(1 for filename in iterate_subtree(subtree_path))


def is_subdirectory(path, directory_path):
    """
    Check that sub_path is ancestor of path
    :param path: root path
    :param directory_path: path of directory that checks on ancestor
    :return: that directory_path is ancestor
    """
    real_directory_path = os.path.realpath(path)
    real_path = os.path.realpath(directory_path)
    return real_path.startswith(real_directory_path)


def pick_non_conflict_name(file_path):
    """
    Find non conflict name of file in subtree
    :param file_path: name of file
    :return: non conflict name of file
    """
    if not os.path.exists(file_path):
        return file_path

    for file_version in itertools.count():
        new_file_path = '{0}.{1}'.format(file_path, file_version)
        if not os.path.exists(new_file_path):
            return new_file_path


def get_file_info(file_path):
    """
    Get detail file info
    :param file_path: path of file
    :return: File class with fields name, size and modify
    """
    filename = os.path.basename(os.path.normpath(file_path))
    file_size = count_subtree_size(file_path)
    file_modify = datetime.datetime.utcfromtimestamp(
        os.path.getatime(file_path)
    )
    file_class = collections.namedtuple('File', 'name size modify')
    return file_class(name=filename, size=file_size, modify=file_modify)
