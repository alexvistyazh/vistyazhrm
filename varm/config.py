#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


""" Load trash config in json and pickle format """


import json
import pickle


def load_config(config_filename):
    """
    Load config for one run only in json or pickle format only in *.json and
    *.pickle extensions.

    :param config_filename: name of config file
    """
    decoder = None

    if config_filename.endswith('.json'):
        decoder = json
    elif config_filename.endswith('.pickle'):
        decoder = pickle
    else:
        raise Exception('Unknown config file format. Should be *.json or *.pickle')

    with open(config_filename, 'r') as config:
        return decoder.load(config)
