#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


""" Parse command line arguments by argparse module. """


import argparse


def get_cli_parser():
    """
    Command line parser based on argparse module
    :return: command line parser
    """

    parser = argparse.ArgumentParser(
        prog='rm',
        description='Smart rm with recovery mode',
    )

    parser.add_argument(
        'files',
        metavar='FILES',
        nargs='*',
        action='store',
        help='Remove file or dir to trash',
    )

    parser.add_argument(
        '--recover',
        '-r',
        action='store',
        nargs='+',
        dest='recover',
        metavar='FILES',
        help='Recover file or dir from trash',
    )

    parser.add_argument(
        '--remove',
        '-R',
        action='store',
        nargs='+',
        dest='remove',
        metavar='FILES',
        help='Remove file or dir from trash',
    )

    parser.add_argument(
        '--list',
        '-l',
        action='store_true',
        dest='list',
        help='Show trash content',
    )

    parser.add_argument(
        '--clear',
        '-c',
        action='store_true',
        dest='clear',
        help='Clear trash',
    )

    parser.add_argument(
        '--path',
        '-p',
        action='store',
        dest='path',
        metavar='PATH',
        help='Set trash path',
    )

    parser.add_argument(
        '--size-limit',
        '-sl',
        action='store',
        type=int,
        dest='size_limit',
        metavar='CAPACITY',
        help='Set trash size limit',
    )

    parser.add_argument(
        '--files-limit',
        '-fl',
        action='store',
        type=int,
        dest='files_limit',
        metavar='CAPACITY',
        help='Set trash files count limit',
    )

    parser.add_argument(
        '--log',
        '-L',
        action='store',
        dest='logfile',
        metavar='FILE',
        help='Set trash logfile',
    )

    parser.add_argument(
        '--dry-run',
        '-dr',
        dest='dry_run',
        action='store_true',
        help='Make operations dummy',
    )

    parser.add_argument(
        '--silent',
        '-s',
        dest='silent',
        action='store_true',
        help='Make operations without output',
    )

    parser.add_argument(
        '--config',
        '-C',
        dest='config',
        action='store',
        help='Custom config for one run',
    )

    return parser
