#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


"""
varm is command line tool which remove file into trash.

Basic varm operations:
    1) insert file/dir on custom trash
    2) remove file/dir from trash
    3) recover file/dir from trash
    4) setup custom trash (path, volume, etc.)
    5) setup one run modes(dry run, silent, stc.)

How it work's:
    1) parse command line arguments
    2) setup logger by this arguments for different modes
    3) setup trash by this arguments
    4) call trash operations

varm package:
    1) parser - command line argument parser based on argparse
    2) logger - setup project loggers and their handlers
    3) config - load custom file config
    4) __main__ - main controller
"""