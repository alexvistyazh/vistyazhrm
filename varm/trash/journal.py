#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


"""
File Journal is a recovery path storage for Trash.

File Journal support operations:
    1) Insert file in File Journal
    2) Remove file if exists from file journal
    3) Get recovery path of file

File Journal structure:
    1) Journal stored on trash 'info' subdirectory
    2) For each trash file exists config with recovery path
    3) Name of trash file config equal name of trash file
"""


import varm.filesystem.operations
import logging
import os


class TrashJournal(object):
    """ Trash Journal store recovery paths of files which put into Trash """

    def __init__(self, journal_path, trash_files_path):
        """
        Init Trash Journal
        :param journal_path: path for storage Trash Journal
        :param trash_files_path: path of trash
        """
        self.journal_path = journal_path
        self.trash_path = trash_files_path

        if not os.path.exists(self.journal_path):
            os.mkdir(self.journal_path)

    def insert(self, filename_in_journal, recovery_path):
        """
        Insert file into Trash Journal with fixed name
        :param filename_in_journal: fixed name of file in Trash Journal
        :param recovery_path: path for recovery from Trash Journal
        """
        path_in_journal = os.path.join(self.journal_path, filename_in_journal)

        with open(path_in_journal, 'w') as journal:
            journal.write(recovery_path)

        logging.getLogger('varm.trash').debug(
            '{0} add to trash journal'.format(recovery_path)
        )

    def remove(self, filename):
        """
        Remove file from Trash Journal or ignore if file not in Trash Journal
        :param filename: name of file in Trash Journal
        """
        path_in_journal = os.path.join(self.journal_path, filename)
        if os.path.exists(path_in_journal):
            os.remove(path_in_journal)
            logging.getLogger('varm.trash').debug(
                '{0} removed from trash journal'.format(filename)
            )

    def handle_move(self, old_path, new_path):
        """
        Handle move event from filesystem module
        and redirect event to Trash Journal remove or insert method
        :param old_path: path before filesystem move operation
        :param new_path: path after filesystem move operation
        """
        if varm.filesystem.operations.is_subdirectory(self.trash_path, old_path):
            filename = os.path.basename(os.path.normpath(old_path))
            self.remove(filename)

        if varm.filesystem.operations.is_subdirectory(self.trash_path, new_path):
            new_filename = os.path.basename(os.path.normpath(new_path))
            self.insert(new_filename, old_path)

    def handle_remove(self, path):
        """
        Handle remove event from filesystem module and
        redirect event to Trash Journal remove
        :param path: path of file which was removed in filesystem
        """
        filename = os.path.basename(os.path.normpath(path))
        self.remove(filename)

    def __contains__(self, filename):
        """
        Check that file is store in trash
        :param filename: name of file in Trash Journal
        :return: That file is stand in trash
        """
        return os.path.exists(os.path.join(self.journal_path, filename))

    def get_recovery_path(self, filename):
        """
        Find recovery path for file in TrashJournal
        :param filename: name of file in Trash Journal
        :return: path for recovery or None if not in Trash Journal
        """
        if filename not in self:
            return None

        with open(os.path.join(self.journal_path, filename)) as journal:
            return journal.read()
