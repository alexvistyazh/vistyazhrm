#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


"""
Trash is temporary folder based storage of removed files.
Trash stored not only removed file, yet stored
recovery paths of removed files on file journal.

Trash operations:
    1) insert file in trash
    2) remove file from trash
    3) recover file from trash
    4) clear trash
    5) view trash content

Trash structure:
    1) trash is a directory
    2) trash has subdirectories 'info' and 'files'
    3) 'files' stored removed files
    4) 'info' stored journal for recovery
    5) name of file in trash usually equal real filename,

File Journal support operations:
    1) Insert file in File Journal
    2) Remove file if exists from file journal
    3) Get recovery path of file

File Journal structure:
    1) Journal stored on trash 'info' subdirectory
    2) For each trash file exists config with recovery path
    3) Name of trash file config equal name of trash file
"""