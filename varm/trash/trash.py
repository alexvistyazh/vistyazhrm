#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


"""
Trash is temporary folder based storage of removed files.

Trash operations:
    1) insert file in trash
    2) remove file from trash
    3) recover file from trash
    4) clear trash
    5) view trash content

Trash structure:
    1) trash is a directory
    2) trash has subdirectories 'info' and 'files'
    3) 'files' stored removed files
    4) 'info' stored journal for recovery
    5) name of file in trash usually equal real filename,
"""


import logging
import os
import glob
import fnmatch
import journal
import varm.filesystem.fileobserver
import varm.filesystem.operations
import varm.config
import varm.trash.settings


DEFAULT_PATH = os.path.expanduser('~/.trash')
DEFAULT_SIZE_LIMIT = 52428800
DEFAULT_FILES_LIMIT = 1000


class Trash(object):
    """
    Trash is temporary storage for files that have been deleted from
    filesystem. Files in trash can be recover to filesystem back or
    removed from trash permanently.
    """

    FILES_DIR = 'files'
    INFO_DIR = 'info'

    def __init__(self, path=DEFAULT_PATH, files_limit=DEFAULT_FILES_LIMIT,
                 size_limit=DEFAULT_SIZE_LIMIT):

        """
        Init Trash instance
        :param path: path for storage Trash
        :param files_limit: limit count files in Trash
        :param size_limit:  limit total size of Trash
        """

        self.path = path

        self.files = os.path.join(self.path, self.FILES_DIR)
        self.info = os.path.join(self.path, self.INFO_DIR)
        self._create_trash_folder()

        self.settings = varm.trash.settings.TrashSettings(
            self.path,
            size_limit=size_limit,
            files_limit=files_limit
        )

        self.logger = logging.getLogger(__name__)
        self.journal = journal.TrashJournal(self.info, self.files)
        varm.filesystem.fileobserver.FileObserver.add_handler(self.journal)

    def _create_trash_folder(self):
        """ Create trash folder structure of trash"""
        for trash_folder in [self.path, self.files, self.info]:
            if not os.path.exists(trash_folder):
                os.mkdir(trash_folder)

    @property
    def available_size_space(self):
        """
        Available size trash property
        :return: free space size that have trash
        """
        space = self.settings.size_limit
        space -= varm.filesystem.operations.count_subtree_size(self.files)
        return space

    @property
    def available_files_space(self):
        """
        Available files trash property
        :return: free space files that have trash
        """
        space = self.settings.files_limit
        space -= varm.filesystem.operations.count_subtree_files(self.files)
        return space

    def is_enough_space_for_insert(self, file_path):
        """
        Check that trash has free space for insert file
        :param file_path: path of file for insert
        :return: enough space or not
        """
        subtree_files = varm.filesystem.operations.count_subtree_files(file_path)
        if self.available_files_space < subtree_files:
            return False

        subtree_size = varm.filesystem.operations.count_subtree_size(file_path)
        if self.available_size_space < subtree_size:
            return False

        return True

    def insert(self, file_path, dry_run=False):
        """
        Insert file or directory to Trash
        :param file_path: path of file which
        :param dry_run: ignore insert operations
        :return inserted paths of files
        """
        if not os.path.exists(file_path):
            self.logger.error('{0} file not exists'.format(file_path))
            return []
        elif not self.is_enough_space_for_insert(file_path):
            self.logger.error('Trash overflow!')
            return []
        else:
            filename = os.path.basename(file_path)
            path_in_trash = varm.filesystem.operations.pick_non_conflict_name(
                os.path.join(self.files, filename)
            )

            return varm.filesystem.operations.move_subtree(
                file_path,
                path_in_trash,
                dry_run,
            )

    def remove(self, filename, dry_run=False):
        """
        Remove file from Trash. Ignore if file not in Trash
        :param filename: name of file in Trash
        :param dry_run: ignore remove operations
        :return removed files
        """
        if filename not in self:
            self.logger.error('{0} not in trash'.format(filename))
            return []
        else:
            path_in_trash = os.path.join(self.files, filename)
            return varm.filesystem.operations.remove_subtree(
                path_in_trash,
                dry_run,
            )

    def recover(self, filename, dry_run=False):
        """
        Recover file or directory from Trash. Ignore if file not in Trash
        :param filename: name of file in Trash
        :param dry_run: ignore recover operations
        :return recovered files
        """
        if filename not in self:
            self.logger.error('{0} not in trash'.format(filename))
            return []
        else:
            path_in_trash = os.path.join(self.files, filename)
            recovery_path = varm.filesystem.operations.pick_non_conflict_name(
                self.journal.get_recovery_path(filename)
            )

            return varm.filesystem.operations.move_subtree(
                path_in_trash,
                recovery_path,
                dry_run,
            )

    def insert_by_unix_pattern(self, unix_pattern, dry_run=False):
        """
        Insert files into Trash by unix-pattern
        :param unix_pattern: unix-pattern for insert files
        :param dry_run: ignore insert operations if value is True
        :return: list of inserted files
        """
        inserted_files = []
        for filename in glob.glob(unix_pattern):
            inserted_files.extend(self.insert(filename, dry_run))

        return inserted_files

    def remove_by_unix_pattern(self, unix_pattern, dry_run=False):
        """
        Remove files from Trash unix-pattern
        :param unix_pattern: unix-pattern for remove files
        :param dry_run: ignore remove operations if value is True
        :return: list of removed files
        """
        removed_files = []
        for filename in fnmatch.filter(self, unix_pattern):
            removed_files.extend(self.remove(filename, dry_run))

        return removed_files

    def recover_by_unix_pattern(self, unix_pattern, dry_run=False):
        """
        Recover files from Trash by unix-pattern
        :param unix_pattern: unix-pattern for recover files
        :param dry_run: ignore recovered operation if value is True
        :return: list of recovered files
        """
        recovered_files = []
        for filename in fnmatch.filter(self, unix_pattern):
            recovered_files.extend(self.recover(filename, dry_run))

        return recovered_files

    def list(self):
        """ Return list of all files in Trash """
        return [
            varm.filesystem.operations.get_file_info(
                os.path.join(self.files, file)
            )
            for file in self
        ]

    def clear(self):
        """ Clear all files from trash """
        for filename in self:
            self.remove(filename)

    def __contains__(self, item):
        """ Check that file placed in Trash """
        return item in self.journal

    def __iter__(self):
        """ Trash iterator"""
        for filename in os.listdir(self.files):
            yield filename
