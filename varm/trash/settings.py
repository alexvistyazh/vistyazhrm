import os


class TrashSettings(object):
    """ Store trash setting for each attribute at folder SETTING_DIR """

    SETTING_DIR = 'settings'

    def __init__(self, trash_path, **settings):
        """
        Init trash settings
        :param trash_path: path of trash
        :param settings: dict with attributes for set up
        """
        self.setting_path = os.path.join(trash_path, self.SETTING_DIR)
        self._create_settings(**settings)

    def _create_settings(self, **settings):
        """
        Create and setup setting structure by setting dict
        :param settings: dict with attributes for set up
        :return: None
        """
        if not os.path.exists(self.setting_path):
            os.mkdir(self.setting_path)
            for key, value in settings.items():
                item_path = os.path.join(self.setting_path, key)
                with open(item_path, 'w') as setting:
                    setting.write(str(value))

    def __getattr__(self, item):
        """
        Give access for settings attributes
        :param item: name of item of setting field
        :return: field value
        """
        try:
            item_path = os.path.join(self.setting_path, item)
            with open(item_path, 'r') as setting:
                return int(setting.read())
        except OSError:
            return None
