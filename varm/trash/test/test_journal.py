#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import collections
import varm.trash.journal
import pytest


@pytest.fixture(scope='function')
def test_journal(tmpdir):

    class DummyJournal(object):

        TEST_FILE = 'test_file'
        TEST_RECOVERY_PATH = 'recovery_path'

        def __init__(self):
            self.folder = tmpdir.mkdir('journal')
            self.instance = varm.trash.journal.TrashJournal(str(self.folder), '')

        def dummy_insert(self, name, recovery_path):
            self.folder.join(name).write(recovery_path)

    return DummyJournal()


@pytest.fixture(scope='function')
def temp_file(tmpdir):
    test_file = tmpdir.join('test_file')
    test_file.write('recovery_path')
    return test_file


def test_exists(test_journal):
    test_journal.dummy_insert(
        test_journal.TEST_FILE,
        test_journal.TEST_RECOVERY_PATH
    )
    assert test_journal.TEST_FILE in test_journal.instance


def test_insert(test_journal, temp_file):
    test_journal.instance.insert(
        test_journal.TEST_FILE,
        str(temp_file)
    )

    assert test_journal.folder.join(test_journal.TEST_FILE).check()


def test_remove(test_journal):
    test_journal.dummy_insert(
        test_journal.TEST_FILE,
        test_journal.TEST_RECOVERY_PATH
    )

    test_journal.instance.remove(test_journal.TEST_FILE)
    assert not test_journal.folder.join(test_journal.TEST_FILE).check()


def test_not_contains(test_journal):
    assert test_journal.TEST_FILE not in test_journal.instance


def test_contains(test_journal):
    test_journal.dummy_insert(
        test_journal.TEST_FILE,
        test_journal.TEST_RECOVERY_PATH
    )

    assert test_journal.TEST_FILE in test_journal.instance


def test_recovery_path(test_journal):
    test_journal.dummy_insert(
        test_journal.TEST_FILE,
        test_journal.TEST_RECOVERY_PATH
    )

    assert test_journal.instance.get_recovery_path(test_journal.TEST_FILE) ==\
           test_journal.TEST_RECOVERY_PATH
