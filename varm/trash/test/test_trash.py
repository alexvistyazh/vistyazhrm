#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import collections
import varm.trash.trash
import pytest


@pytest.fixture(scope='function')
def test_trash(tmpdir):
    trash_directory = tmpdir.mkdir('trash')

    class DummyTrash(object):

        TEST_FILE = 'test_file'
        TEST_RECOVERY_PATH = tmpdir.join('recovery_path')

        def __init__(self):
            self.real_instance = varm.trash.trash.Trash(str(trash_directory))
            self.files = trash_directory.join('files')
            self.journal = trash_directory.join('info')

        def dummy_insert(self, name, recovery_path):
            self.files.join(name).write('')
            self.journal.join(name).write(recovery_path)

        def dummy_check_recovery(self, recovery_path):
            return recovery_path.check()

    return DummyTrash()


@pytest.fixture(scope='function')
def temp_file(tmpdir):
    test_file = tmpdir.join('test_temp_file')
    test_file.write('test_temp_file')
    return test_file


def test_insert(test_trash, temp_file):
    test_trash.real_instance.insert(str(temp_file))
    assert test_trash.journal.join(temp_file.basename).check()
    assert not temp_file.check()


def test_insert_dry_run(test_trash, temp_file):
    test_trash.real_instance.insert(str(temp_file), dry_run=True)
    assert not test_trash.journal.join(temp_file.basename).check()
    assert temp_file.check()


def test_remove(test_trash):
    test_trash.dummy_insert(test_trash.TEST_FILE, test_trash.TEST_RECOVERY_PATH)
    test_trash.real_instance.remove(test_trash.TEST_FILE)
    assert not test_trash.journal.join(test_trash.TEST_FILE).check()


def test_remove_dry_run(test_trash):
    test_trash.dummy_insert(test_trash.TEST_FILE, test_trash.TEST_RECOVERY_PATH)
    test_trash.real_instance.remove(test_trash.TEST_FILE, dry_run=True)
    assert test_trash.journal.join(test_trash.TEST_FILE).check()


def test_recover(test_trash):
    test_trash.dummy_insert(test_trash.TEST_FILE, test_trash.TEST_RECOVERY_PATH)
    test_trash.real_instance.recover(test_trash.TEST_FILE)
    assert not test_trash.journal.join(test_trash.TEST_FILE).check()
    assert test_trash.dummy_check_recovery(test_trash.TEST_RECOVERY_PATH)


def test_recover_dry_run(test_trash):
    test_trash.dummy_insert(test_trash.TEST_FILE, test_trash.TEST_RECOVERY_PATH)
    test_trash.real_instance.recover(test_trash.TEST_FILE, dry_run=True)
    assert test_trash.journal.join(test_trash.TEST_FILE).check()
    assert not test_trash.dummy_check_recovery(test_trash.TEST_RECOVERY_PATH)


def test_clear(test_trash):
    test_trash.dummy_insert(test_trash.TEST_FILE, test_trash.TEST_RECOVERY_PATH)
    test_trash.real_instance.clear()
    assert len(list(test_trash.files.visit())) == 0


def test_contains(test_trash):
    test_trash.dummy_insert(test_trash.TEST_FILE, test_trash.TEST_RECOVERY_PATH)
    assert test_trash.TEST_FILE in test_trash.real_instance


def test_not_contains(test_trash):
    assert test_trash.TEST_FILE not in test_trash.real_instance


def test_limits(test_trash, temp_file):
    test_trash.real_instance.files_limit = 0
    test_trash.real_instance.insert(str(temp_file))
    assert not test_trash.files.join(temp_file.basename).check()
