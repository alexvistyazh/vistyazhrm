#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


""" Setup logger for silent and regular modes by setup_logger method """


import logging
import sys
import os


DEFAULT_LOG_NAME = os.path.expanduser('~/.trashlog')
DEFAULT_LOG_FORMAT = '%(asctime)s [%(levelname)s] %(message)s'


def setup_logger_handler(handler_instance):
    """
    Setup logger handler instance default formatter and level
    :param handler_instance: instance of logger handler
    :return: setup handler instance
    """
    handler_instance.setFormatter(logging.Formatter(DEFAULT_LOG_FORMAT))
    handler_instance.setLevel(logging.DEBUG)
    return handler_instance


def setup_logger(logger, logfile=None, silent=False):
    """
    Setup logger silent or regular mode
    :param logger: logger instance for setup
    :param logfile: name of logfile
    :param silent: Mode without output
    """
    logger.setLevel(logging.DEBUG)

    if not logfile:
        logfile = DEFAULT_LOG_NAME

    if silent:
        logger.handlers = [
            logging.NullHandler(),
        ]

    else:
        logger.handlers = [
            setup_logger_handler(logging.FileHandler(logfile)),
            setup_logger_handler(logging.StreamHandler(sys.stderr)),
        ]
