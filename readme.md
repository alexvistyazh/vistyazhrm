# VARM #

varm is a command line tool which remove files to custom
[trash](https://en.wikipedia.org/wiki/Trash_(computing)).


## SUPPORT OPERATIONS ##
* move file(s) to custom trash
* recover files(s) from custom trash
* remove file(s) from trash
* view all trash files
* setup mode (dry-run, silent, etc.) for each tool run
* config custom trash by config file or by command line arguments

## PACKAGE STRUCTURE ##
* filesystem - implement basic filesystem operations and filesystem observer
* trash - implement trash class and trash journal class
* test - project's tests

## INSTALL ##
`git clone https://alexvistyazh@bitbucket.org/alexvistyazh/vistyazhrm.git`

`python setup.py install`

## DEPENDENCIES ##
* Python 2.7

## BASIC USAGE EXAMPLES ##
Remove files to trash:

`varm '/home/user/*.txt'`

Show all trash files:

`varm --list`

Recover files from trash:

`varm --recover '*.txt'`

Remove files from trash:

`varm --remove '*.txt'`
