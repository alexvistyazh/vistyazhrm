#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-


import setuptools


setuptools.setup(
    name='varm',
    version='1.0',
    author='Alex Vistyazh',
    author_email='alexvistyazh@gmail.com',
    description='CLI tool for removing files to custom trash',
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'varm = varm.__main__:main',
        ]
    }
)
